#! /bin/bash
set -u

kernelList=`dpkg --get-selections | egrep  "linux-headers-[0-9]+\.[0-9]+\.[0-9]+-[0-9]+-" | awk '{print$1}'`

kernelXYZ=()
kernelXYZ_L=()

for kernel in $kernelList
do
    array=(`echo $kernel | tr '-' ' '`)
    id=${#kernelXYZ[*]}
    kernelXYZ["$id"]=${array[2]}
    kernelXYZ_L["$id"]=${array[3]}
done

# echo -e "\033[41m 注意：deepin官方内核5.3.0请勿卸载 \033[0m"
echo
echo -e "\033[42m 当前已安装内核列表：\033[0m"
echo "-------------------"
for ((i=0;i<${#kernelXYZ[@]};i++))
do
    echo "┊序号 $i ┊  ${kernelXYZ[$i]} ┊"
    echo "-------------------"
done

read -p "输入数字序号卸载对应内核：" num
if [ $num -ge ${#kernelXYZ[@]} ];then
    echo "输入有误,请输入正确的序号"
    exit -1
fi
echo -e "正要卸载内核 \033[44;37m ${kernelXYZ[$num]} \033[0m 输入您的密码以继续..."
sudo apt remove --purge linux-headers-${kernelXYZ[$num]}-${kernelXYZ_L[$num]}
sudo apt remove --purge linux-modules-${kernelXYZ[$num]}-${kernelXYZ_L[$num]}-generic



